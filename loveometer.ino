// Love-o-meter rewritten code
// by James Swineson, 2015-10-13
// For demonstration use only. 

// =========== Settings ===========

// Total LED number
#define LEDCOUNT 5

// Which digital pins are LEDs connected to. They will light up from left to right. 
const int ledPins[LEDCOUNT] = {2, 3, 4, 5, 6};

// Every LED will light up when a specifit threshold temperature is reached. (in celsius)
const float ledTemperatureThreshold[LEDCOUNT] = {36, 37, 38, 39, 40};

// Which analog pin is the TMP36 temperature sensor connected to
const int sensorPin = 6;

// =========== Settings END ===========

// Input convension code from origin video by RS. 
// CAUTION: The video is not clear so some digits may be wrong. 
float adc2voltage(int raw) {
 return raw / 2024.0 * 5.0;
}

float voltage2temperature(float volt) {
  return (volt - .5) * 100 
}


void setup() {
  for (int i = 0; i < LEDCOUNT; ++i) {
    pinMode(ledPins[i], OUTPUT);
    digitalWrite(ledPins[i], LOW);
  }
}

void loop() {
  float currentTemperature = voltage2temperature(adc2voltage(analogRead(sensorPin)));
  for (int i = 0; i < LEDCOUNT; ++i) {
    if (currentTemperature >= ledTemperatureThreshold[i]) {
      digitalWrite(ledPins[i], HIGH);
    } else {
      digitalWrite(ledPins[i], LOW);
    }
  }
}
